msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-28 00:25+0200\n"
"PO-Revision-Date: 2023-11-23 01:09+0000\n"
"Last-Translator: capitalthree <alexbobp@gmail.com>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/minetest/"
"nodecore/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2.1-rc\n"

msgid "- FUTURE: @1"
msgstr "- FUTURO: @1"

msgid "- Learn to use the stars for long distance navigation."
msgstr "- Aprende a usar las estrellas para navegación a larga distancia."

msgid "- Trouble lighting a fire? Try using longer sticks, more tinder."
msgstr ""
"- ¿Problemas para prender fuego? Intenta utilizar palos más largos, o más "
"yesca facil de encender."

msgid "- Tools used as ingredients must be in very good condition."
msgstr ""
"- Las herramientas utilizadas como ingredientes deben estar en muy buena "
"condición."

msgid "- Drop and pick up items to rearrange your inventory."
msgstr "- Suelta y recoge objetos para reorganizar tu inventario."

msgid "- Larger recipes are usually more symmetrical."
msgstr "- Los recipientes grandes usualmente son más simétricos."

msgid "- There is NO inventory screen."
msgstr "- NO hay pantalla de inventario."

msgid "(C)2018-@1 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-@1 por Aaron Suen <warr1024@@gmail.com>"

msgid "- @1"
msgstr "- @1"

msgid "- Aux+drop any item to drop everything."
msgstr "- Aux+soltar cualquier item para soltar todo."

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- Los \"hornos\" no existen; Descubre la fundición con llamas abiertas."

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr "- ¿No puedes romper árboles o pasto? Busca palos en las hojas."

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr ""
"- Ten cuidado con cuevas y abismos oscuros. Sólo tú tienes la "
"responsabilidad de salir."

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr ""
"- Los puntos de accesso a escalar también producen una luz muy tenue; sube "
"el gamma de la pantalla para ver lo mejor."

msgid "- Climbing spots may be climbed once black particles appear."
msgstr ""
"- Puedes escalar los puntos de accesso una vez que las partículas negras "
"apparecen."

msgid "- DONE: @1"
msgstr "- COMPLETO: @1"

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr ""
"- Suelta objetos en el piso para crear montones. Los montones no desaparecen."

msgid "- Hold/repeat right-click on walls/ceilings barehanded to create climbing spots."
msgstr ""
"- Sostenga/repita el right-click en las paredes/techos con tu mano vacía "
"para crear puntos de accesso a escalar."

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr ""
"- Los nodos desplazados se pueden escalar como si fueran puntos de accesso a "
"escalar."

msgid "- Crafting is done by building recipes in-world."
msgstr ""
"- La fabricación se realiza siguendo los planos dentro del espacio físico "
"del juego."

msgid "- For larger recipes, the center item is usually placed last."
msgstr ""
"- Para planos más grandes, en general, el objeto de en medio se coloca al "
"final."
