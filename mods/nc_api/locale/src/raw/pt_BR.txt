msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-12 14:44+0000\n"
"PO-Revision-Date: 2023-11-04 02:32+0000\n"
"Last-Translator: Terifo <terifo1590@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/"
"minetest/nodecore/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.2-dev\n"

msgid "(C)2018-2019 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2019 por Aaron Suen <war1024@@gmail.com>"

msgid "- Crafting is done by building recipes in-world."
msgstr "- A Criação de Itens é feita construindo as receitas pelo mundo."

msgid "- Drop items onto ground to create stack nodes.  They do not decay."
msgstr "- Solte itens no chão para criar um node de itens amontoados. Eles não desaparecem."

msgid "- If a recipe exists, you will see a special particle effect."
msgstr "- Se uma receita existe, você verá um efeito especial de partícula sair do node."

msgid "- Items picked up try to fit into the current selected slot first."
msgstr "- Os itens pegados são primeiramente armazenados no espaço atualmente selecionado."

msgid "- Order and specific face of placement may matter for crafting."
msgstr "- A ordem e a face em que os itens são colocados pode importar no processo de criação."

msgid "- Recipes are time-based, punching faster does not speed up."
msgstr "- Receitas são cronometradas, bater mais forte não afetará a velocidade delas."

msgid "- Sneak+drop to count out single items from stack."
msgstr "- Use os comandos de agachar e soltar juntos para soltar um só item."

msgid "- Some recipes require "pummeling" a node."
msgstr "- Algumas receitas necessitam que você "esmurre" um node."

msgid "- Stacks may be pummeled, exact item count may matter."
msgstr "- Amontoados de itens podem ser esmurrados, pode ser que precise de uma quantidade exata do item."

msgid "- There is NO inventory screen."
msgstr "- NÃO EXISTE tela de inventário."

msgid "- To pummel, punch a node repeatedly, WITHOUT digging."
msgstr "- Para esmurrar, bata num node repetidamente, SEM QUEBRÁ-LO."

msgid "- Wielded item, target face, and surrounding nodes may matter."
msgstr "- O item segurado, a face do objeto alvo e os nodes ao redor podem importar durante a criação de algo."

msgid "- You do not have to punch very fast (about 1 per second)."
msgstr "- Você não precisa bater muito rápido(basta 1 batida por segundo)."

msgid "...and 1 more hint..."
msgstr "...e mais 1 dica..."

msgid "...and @1 more hints..."
msgstr "...e mais @1 dicas..."

msgid "...have you activated a lens yet?"
msgstr "...você já ativou uma lente?"

msgid "...have you assembled a staff from sticks yet?"
msgstr "...você já montou um bastão usando gravetos?"

msgid "...have you assembled a wooden frame yet?"
msgstr "...você já montou um estrutura de madeira?"

msgid "...have you assembled a wooden ladder yet?"
msgstr "...você já montou uma escada de madeira?"

msgid "...have you assembled a wooden shelf yet?"
msgstr "...você já montou uma estante de madeira?"

msgid "...have you assembled a wooden tool yet?"
msgstr "...você já montou uma ferramenta de madeira?"

msgid "...have you assembled an adze out of sticks yet?"
msgstr "...você já montou um enxó usando gravetos?"

msgid "...have you assembled an annealed lode tote handle yet?"
msgstr "...você já montou uma alça de bolsa recozida?"

msgid "...have you bashed a plank into sticks yet?"
msgstr "...você já quebrou uma tábua em gravetos batendo nela?"

msgid "...have you broken cobble into chips yet?"
msgstr "...você já quebrou pedregulho em pedaços?"

msgid "...have you carved a wooden plank completely yet?"
msgstr "...você já entalhou completamente uma tábua de madeira?"

msgid "...have you carved wooden tool heads from planks yet?"
msgstr "...você já transformou uma tábua em cabeças de ferramentas, entalhando a madeira?"

msgid "...have you chipped chromatic glass into prisms yet?"
msgstr "...você já quebrou um vidro cromático em prismas, lascando o vidro?"

msgid "...have you chopped a lode cube into prills yet?"
msgstr "...você já quebrou um veio mineral em lascas, usando um machado?"

msgid "...have you chopped chromatic glass into lenses yet?"
msgstr "...você já quebrou vidro cromático em lentes, usando um machado?"

msgid "...have you chopped up charcoal yet?"
msgstr "...você já quebrou carvão usando um machado?"

msgid "...have you cold-forged an annealed lode tool head yet?"
msgstr "...você já fez uma forja fria de uma cabeça de ferramenta de veio mineral?"

msgid "...have you cold-forged lode down completely yet?"
msgstr "...você já fez uma forja fria de um veio mineral?"

msgid "...have you cooled molten glass into crude glass yet?"
msgstr "...você já esfriou vidro derretido para que este virasse vidro cru?"

msgid "...have you cut down a tree yet?"
msgstr "...você já cortou uma árvore?"

msgid "...have you dug up a tree stump yet?"
msgstr "...você já desenterrou um toco de árvore?"

msgid "...have you dug up dirt yet?"
msgstr "...você já cavou terra?"

msgid "...have you dug up gravel yet?"
msgstr "...você já cavou cascalho?"

msgid "...have you dug up lode ore yet?"
msgstr "...você já desenterrou um veio mineral?"

msgid "...have you dug up sand yet?"
msgstr "...você já cavou areia?"

msgid "...have you dug up stone yet?"
msgstr "...você já cavou pedra?"

msgid "...have you found a lode stratum yet?"
msgstr "...você já encontrou um estrato de veio mineral?"

msgid "...have you found ash yet?"
msgstr "...você já encontrou cinzas?"

msgid "...have you found charcoal yet?"
msgstr "...você já encontrou carvão?"

msgid "...have you found deep stone strata yet?"
msgstr "...você já encontrou um estrato de pedra profunda?"

msgid "...have you found dry leaves yet?"
msgstr "...você já encontrou folhas secas?"

msgid "...have you found eggcorns yet?"
msgstr "...você já encontrou semente de carvalho?"

msgid "...have you found lode ore yet?"
msgstr "...você já encontrou um veio mineral?"

msgid "...have you found molten rock yet?"
msgstr "...você já encontrou pedra derretida (lava)?"

msgid "...have you found sponges yet?"
msgstr "...você já encontrou esponjas?"

msgid "...have you found sticks yet?"
msgstr "...você já encontrou gravetos?"

msgid "...have you made fire by rubbing sticks together yet?"
msgstr "...você já fez fogo esfregando gravetos?"

msgid "...have you melted down lode metal yet?"
msgstr "...você já derreteu um veio mineral?"

msgid "...have you melted sand into glass yet?"
msgstr "...você já transformou areia em vidro, derretendo a areia?"

msgid "...have you molded molten glass into clear glass yet?"
msgstr "...você já moldou vidro derretido em vidro transparente?"

msgid "...have you packed high-quality charcoal yet?"
msgstr "...você já juntou carvão de alta qualidade?"

msgid "...have you packed stone chips back into cobble yet?"
msgstr "...você já juntou pedaços de pedra para que virassem pedregulho?"

msgid "...have you planted an eggcorn yet?"
msgstr "...você já plantou uma semente de carvalho?"

msgid "...have you produced light from a lens yet?"
msgstr "...você já gerou luz a partir de uma lente?"

msgid "...have you put a stone tip onto a tool yet?"
msgstr "...você já colocou uma ponta de pedra em uma ferramenta?"

msgid "...have you quenched molten glass into chromatic glass yet?"
msgstr "...você já temperou vidro derretido para que virasse vidro cromático?"

msgid "...have you sintered glowing lode into a cube yet?"
msgstr "...você já moldou veio mineral incandescente em um cubo?"

msgid "...have you split a tree trunk into planks yet?"
msgstr "...você já fez tábuas quebrando troncos de madeira?"

msgid "...have you tempered a lode anvil yet?"
msgstr "...você já fez uma bigorna temperando veio mineral?"

msgid "...have you tempered a lode tool head yet?"
msgstr "...você já fez uma cabeça de ferramenta temperando veio mineral?"

msgid "...have you welded a lode pick and spade together yet?"
msgstr "...você já soldou uma picareta de veio mineral com uma pá de veio mineral?"

msgid "About"
msgstr "Sobre"

msgid "Active Lens"
msgstr "Lente Ativa"

msgid "Active Prism"
msgstr "Prisma Ativo"

msgid "Aggregate"
msgstr "Agregado"

msgid "Air"
msgstr "Ar"

msgid "Annealed Lode"
msgstr "Veio Mineral Recozido"

msgid "Annealed Lode Bar"
msgstr "Barra de Veio Mineral Recozida"

msgid "Annealed Lode Hatchet"
msgstr "Machado de Veio Mineral Recozida"

msgid "Annealed Lode Hatchet Head"
msgstr "Cabeça de Machado de Veio Mineral Recozida"

msgid "Annealed Lode Mallet"
msgstr "Marreta de Veio Mineral Recozida"

msgid "Annealed Lode Mallet Head"
msgstr "Cabeça de Marreta de Veio Mineral Recozida"

msgid "Annealed Lode Mattock"
msgstr "Enxada de Veio Mineral Recozida"

msgid "Annealed Lode Mattock Head"
msgstr "Cabeça de Enxada de Veio Mineral Recozida"

msgid "Annealed Lode Pick"
msgstr "Picareta de Veio Mineral Recozida"

msgid "Annealed Lode Pick Head"
msgstr "Cabeça de Picareta de Veio Mineral Recozida"

msgid "Annealed Lode Prill"
msgstr "Lascas de Veio Mineral Recozida"

msgid "Annealed Lode Rod"
msgstr "Vara de Veio Mineral Recozida"

msgid "Annealed Lode Spade"
msgstr "Pá de Veio Mineral Recozida"

msgid "Annealed Lode Spade Head"
msgstr "Cabeça de Pá de Veio Mineral Recozida"

msgid "Ash"
msgstr "Cinza"

msgid "Ash Lump"
msgstr "Nódulo de Cinzas"

msgid "Burning Embers"
msgstr "Brasa Quente"

msgid "Charcoal"
msgstr "Carvão Vegetal"

msgid "Charcoal Lump"
msgstr "Nódulo de Carvão Vegetal"

msgid "Chromatic Glass"
msgstr "Vidro Cromático"

msgid "Clear Glass"
msgstr "Vidro Transparente"

msgid "Cobble"
msgstr "Pedregulho"

msgid "Cobble Hinged Panel"
msgstr "Painel de Pedregulho Articulado"

msgid "Cobble Panel"
msgstr "Painel de Pedregulho"

msgid "Crude Glass"
msgstr "Vidro Bruto"

msgid "DEVELOPMENT VERSION"
msgstr "VERSÃO EM DESENVOLVIMENTO"

msgid "Dirt"
msgstr "Terra"

msgid "Discord:   https://discord.gg/SHq2tkb"
msgstr "Discord:   https://discord.gg/SHq2tkb"

msgid "Eggcorn"
msgstr "Semente de Carvalho"

msgid "Fire"
msgstr "Fogo"

msgid "Float Glass"
msgstr "Vidro Float"

msgid "GitLab:    https://gitlab.com/sztest/nodecore"
msgstr "GitLab:    https://gitlab.com/sztest/nodecore"

msgid "Glowing Lode"
msgstr "Veio Mineral Incandescente"

msgid "Glowing Lode Bar"
msgstr "Barra de Veio Mineral Incandescente"

msgid "Glowing Lode Hatchet"
msgstr "Machado de Veio Mineral Incandescente"

msgid "Glowing Lode Hatchet Head"
msgstr "Cabeça de Machado Veio Mineral Incandescente"

msgid "Glowing Lode Mallet"
msgstr "Marreta de Veio Mineral Incandescente"

msgid "Glowing Lode Mallet Head"
msgstr "Cabeça de Marreta Veio Mineral Incandescente"

msgid "Glowing Lode Mattock"
msgstr "Enxada de Veio Mineral Incandescente"

msgid "Glowing Lode Mattock Head"
msgstr "Cabeça de Enxada Veio Mineral Incandescente"

msgid "Glowing Lode Pick"
msgstr "Picareta de Veio Mineral Incandescente"

msgid "Glowing Lode Pick Head"
msgstr "Cabeça de Picareta de Veio Mineral Incandescente"

msgid "Glowing Lode Prill"
msgstr "Lascas de Veio Mineral Incandescente"

msgid "Glowing Lode Rod"
msgstr "Vara de Veio Mineral Incandescente"

msgid "Glowing Lode Spade"
msgstr "Pá de Veio Mineral Incandescente"

msgid "Glowing Lode Spade Head"
msgstr "Cabeça de Pá Veio Mineral Incandescente"

msgid "Grass"
msgstr "Grama"

msgid "Gravel"
msgstr "Cascalho"

msgid "Hints"
msgstr "Dicas"

msgid "Ignore"
msgstr "Ignorar"

msgid "Injury"
msgstr "Ferir"

msgid "Inventory"
msgstr "Inventário"

msgid "Leaves"
msgstr "Folhas"

msgid "Lens"
msgstr "Lente"

msgid "Living Sponge"
msgstr "Esponja Viva"

msgid "Lode Cobble"
msgstr "Veio de Pedregulho"

msgid "Lode Ore"
msgstr "Veio Mineral"

msgid "Loose Cobble"
msgstr "Pedregulho Solta"

msgid "Loose Dirt"
msgstr "Terra Solta"

msgid "Loose Gravel"
msgstr "Cascalho Solto"

msgid "Loose Leaves"
msgstr "Folhas Soltas"

msgid "Loose Lode Cobble"
msgstr "Veio de Pedregulho Solto"

msgid "Loose Sand"
msgstr "Areia Solta"

msgid "MIT License:  http://www.opensource.org/licenses/MIT"
msgstr "Licensa MIT: http://www.opensource.org/licenses/MIT"

msgid "Molten Glass"
msgstr "Vidro Derretido"

msgid "Molten Rock"
msgstr "Pedra Derretida"

msgid "NodeCore"
msgstr "NodeCore"

msgid "Not all game content is covered by hints.  Explore!"
msgstr "As dicas não cobrem todo o conteúdo do jogo. Explore!"

msgid "Player's Guide: Inventory Management"
msgstr "Guia do Jogador: Gerenciamento de Inventário"

msgid "Player's Guide: Pummeling Recipes"
msgstr "Guia do Jogador: Receitas por Esmurrada"

msgid "Prism"
msgstr "Prisma"

msgid "Progress: @1 complete, @2 current, @3 future"
msgstr "Progresso: @1 completo(s), @2 atual(is), @3 futuro(s)"

msgid "Pummel"
msgstr "Bater"

msgid "Sand"
msgstr "Areia"

msgid "See included LICENSE file for full details and credits"
msgstr "Veja o arquivo de LICENSA incluso para créditos e maiores detalhes"

msgid "Shining Lens"
msgstr "Lente Brilhante"

msgid "Sponge"
msgstr "Esponja"

msgid "Staff"
msgstr "Bastão"

msgid "Stick"
msgstr "Graveto"

msgid "Stone"
msgstr "Pedra"

msgid "Stone Chip"
msgstr "Pedaço de Pedra"

msgid "Stone-Tipped Hatchet"
msgstr "Machado com Ponta de Pedra"

msgid "Stone-Tipped Mallet"
msgstr "Marreta com Ponta de Pedra"

msgid "Stone-Tipped Pick"
msgstr "Picareta com Ponta de Pedra"

msgid "Stone-Tipped Spade"
msgstr "Pá com Ponta de Pedra"

msgid "Stump"
msgstr "Toco"

msgid "Teleport to get unstuck (but you can't bring your items)"
msgstr "Teleporte para desprender-se (mas você perderá seus itens)"

msgid "Tempered Lode"
msgstr "Veio Mineral Temperado"

msgid "Tempered Lode Bar"
msgstr "Barra de Veio Mineral Temperado"

msgid "Tempered Lode Hatchet"
msgstr "Machado de Veio Mineral Temperado"

msgid "Tempered Lode Hatchet Head"
msgstr "Cabeça de Machado de Veio Mineral Temperado"

msgid "Tempered Lode Mallet"
msgstr "Marreta de Veio Mineral Temperado"

msgid "Tempered Lode Mallet Head"
msgstr "Cabeça de Marreta de Veio Mineral Temperado"

msgid "Tempered Lode Mattock"
msgstr "Enxada de Veio Mineral Temperado"

msgid "Tempered Lode Mattock Head"
msgstr "Cabeça de Enxada de Veio Mineral Temperado"

msgid "Tempered Lode Pick"
msgstr "Picareta de Veio Mineral Temperado"

msgid "Tempered Lode Pick Head"
msgstr "Cabeça de Picareta de Veio Mineral Temperado"

msgid "Tempered Lode Prill"
msgstr "Lascas de Veio Mineral Temperado"

msgid "Tempered Lode Rod"
msgstr "Vara de Veio Mineral Temperado"

msgid "Tempered Lode Spade"
msgstr "Pá de Veio Mineral Temperado"

msgid "Tempered Lode Spade Head"
msgstr "Cabeça de Enxada de Veio Mineral Temperado"

msgid "Tote (1 Slot)"
msgstr "Bolsa (1 Compartimento)"

msgid "Tote (2 Slots)"
msgstr "Bolsa (2 Compartimentos)"

msgid "Tote (3 Slots)"
msgstr "Bolsa (3 Compartimentos)"

msgid "Tote (4 Slots)"
msgstr "Bolsa (4 Compartimentos)"

msgid "Tote (5 Slots)"
msgstr "Bolsa (5 Compartimentos)"

msgid "Tote (6 Slots)"
msgstr "Bolsa (6 Compartimentos)"

msgid "Tote (7 Slots)"
msgstr "Bolsa (7 Compartimentos)"

msgid "Tote (8 Slots)"
msgstr "Bolsa (8 Compartimentos)"

msgid "Tote Handle"
msgstr "Alça de Bolsa"

msgid "Tree Trunk"
msgstr "Tronco de Árvore"

msgid "Unknown Item"
msgstr "Item Desconhecido"

msgid "Version"
msgstr "Versão"

msgid "Water"
msgstr "Água"

msgid "Wet Aggregate"
msgstr "Massa Úmida"

msgid "Wet Sponge"
msgstr "Esponja Molhada"

msgid "Wooden Adze"
msgstr "Enxó de Madeira"

msgid "Wooden Frame"
msgstr "Estrutura de Madeira"

msgid "Wooden Hatchet"
msgstr "Machado de Madeira"

msgid "Wooden Hatchet Head"
msgstr "Cabeça de Machado de Madeira"

msgid "Wooden Hinged Panel"
msgstr "Painel Articulado de Madeira"

msgid "Wooden Ladder"
msgstr "Escada de Madeira"

msgid "Wooden Mallet"
msgstr "Marreta de Madeira"

msgid "Wooden Mallet Head"
msgstr "Cabeça de Marreta de Madeira"

msgid "Wooden Panel"
msgstr "Painel de Madeira"

msgid "Wooden Pick"
msgstr "Picareta de Madeira"

msgid "Wooden Pick Head"
msgstr "Cabeça de Picareta de Madeira"

msgid "Wooden Plank"
msgstr "Tábua de Madeira"

msgid "Wooden Shelf"
msgstr "Estante de Madeira"

msgid "Wooden Spade"
msgstr "Pá de Madeira"

msgid "Wooden Spade Head"
msgstr "Cabeça de Pá de Madeira"

msgid "https://content.minetest.net/packages/Warr1024/nodecore/"
msgstr "https://content.minetest.net/packages/Warr1024/nodecore/"

msgid "- The game is challenging by design, sometimes frustrating. DON'T GIVE UP!"
msgstr ""
"- O jogo é feito para ser desafiante, algumas fezes frustrante. NÃO DESISTA!"

msgid "- Ores may be hidden, but revealed by subtle clues in terrain."
msgstr ""
"- Minérios talvez estejam escondidos, mas podem ser revelados por dicas "
"sutis pelo terreno."

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr "- Derrube itens no chão para criar pilhas de nodes. Eles não somem."

msgid "- DONE: @1"
msgstr "- DONE: @1"

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr ""
"- Não consegue quebrar árvores ou cavar? Procure por gravetos na cobertura."

msgid "- @1"
msgstr "- @1"

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr ""
"-\"Fornalhas\" não existem; descubra como cozinhar com chamas à céu aberto."

msgid "(and @1 more hints)"
msgstr "(e mais @1 dica)"

msgid "(and 1 more hint)"
msgstr "(e mais 1 dica)"

msgid "chop up charcoal"
msgstr "martele carvão mineral"

msgid "chop chromatic glass into lenses"
msgstr "martele vidro cromático tornando-o lentes"

msgid "chop a lode crate back apart"
msgstr "quebre uma caixa de veio mineral"

msgid "chop a glowing lode cube into prills"
msgstr "martele um cubo de veio mineral incandescente até torná-lo granulado"

msgid "chisel a hinge groove into cobble"
msgstr "talhe um espaço para dobradiça em um pedregulho"

msgid "chisel a hinge groove into a wooden plank"
msgstr "talhe um espaço para dobradiça em uma tábua de madeira"

msgid "chip chromatic glass into prisms"
msgstr "lasque vidro cromado em prismas"

msgid "carve wooden tool heads from planks"
msgstr "entalhe cabeças de ferramentas de madeira a partir de tábuas"

msgid "carve a wooden plank completely"
msgstr "entalhe uma tábua de madeira completamente"

msgid "break cobble into chips"
msgstr "quebre pedregulho em lascas"

msgid "bash a plank into sticks"
msgstr "quebre uma tábua em gravetos"

msgid "assemble an annealed lode tote handle"
msgstr "monte uma alça de bolsa de veio mineral recozido"

msgid "assemble an adze out of sticks"
msgstr "monte um enxó a partir de gravetos"

msgid "assemble a wooden tool"
msgstr "monte uma ferramenta de madeira"

msgid "assemble a wooden shelf from frames and planks"
msgstr "monde uma estante de madeira a partir de suportes e tábuas"

msgid "assemble a wooden ladder from sticks"
msgstr "monte uma escada de madeira a partir de gravetos"

msgid "assemble a wooden frame from staves"
msgstr "monte um suporte de madeira a partir de bastões"

msgid "assemble a staff from sticks"
msgstr "monte um bastão a partir de gravetos"

msgid "activate a lens"
msgstr "ative uma lente"

msgid "Torch"
msgstr "Tocha"

msgid "Tips"
msgstr "Dicas"

msgid "Rake"
msgstr "Ancinho"

msgid "Player's Guide: Tips and Guidance"
msgstr "Guia do Jogador: Dicas e Orientações"

msgid "Peat"
msgstr "Turfa"

msgid "Not all game content is covered by hints. Explore!"
msgstr "Nem todo o conteúdo do jogo é coberto pelas dicas. Explore!"

msgid "MIT License (http://www.opensource.org/licenses/MIT)"
msgstr "Licença MIT (http://www.opensource.org/licenses/MIT)"

msgid "Lux Flow"
msgstr "Fluxo de Lux"

msgid "Lux Cobble"
msgstr "Pedregulho de Lux"

msgid "Loose Lux Cobble"
msgstr "Pedregulho de Lux Solto"

msgid "Loose Humus"
msgstr "Húmus Solto"

msgid "Log"
msgstr "Tora"

msgid "Lode Crate"
msgstr "Caixa de Veio Mineral"

msgid "Lit Torch"
msgstr "Acender Tocha"

msgid "Infused Tempered Lode Spade"
msgstr "Pá Infundida de Veio Mineral Temperado"

msgid "Infused Tempered Lode Pick"
msgstr "Picareta Infundida de Veio Mineral Temperado"

msgid "Infused Tempered Lode Mattock"
msgstr "Alvião Infundida de Veio Mineral Temperado"

msgid "Infused Tempered Lode Mallet"
msgstr "Marreta Infundida de Veio Mineral Temperado"

msgid "Infused Tempered Lode Hatchet"
msgstr "Machado Infundido de Veio Mineral Temperado"

msgid "Infused Annealed Lode Spade"
msgstr "Pá Infundida de Veio Mineral Recozido"

msgid "Infused Annealed Lode Pick"
msgstr "Picareta Infundida de Veio Mineral Recozido"

msgid "Infused Annealed Lode Mattock"
msgstr "Enxada Infundida de Veio Mineral Recozido"

msgid "Infused Annealed Lode Mallet"
msgstr "Marreta Infundida de Veio Mineral Recozido"

msgid "Infused Annealed Lode Hatchet"
msgstr "Machado Infundido de Veio Mineral Recozido"

msgid "Humus"
msgstr "Húmus"

msgid "Glowing Lode Cobble"
msgstr "Lasca de Veio Mineral Incandescente"

msgid "GitLab: https://gitlab.com/sztest/nodecore"
msgstr "GitLab: https://gitlab.com/sztest/nodecore"

msgid "Discord: https://discord.gg/SHq2tkb"
msgstr "Discord: https://discord.gg/SHq2tkb"

msgid "Charcoal Glyph"
msgstr "Glifo de Carvão Vegetal"

msgid "Burn"
msgstr "Queimadura"

msgid "- Trouble lighting a fire? Try using longer sticks, more tinder."
msgstr ""
"- Problemas ao acender uma chama? Tente usar gravetos mais longos, mais "
"inflamáveis."

msgid "- Stuck in a pit? Hold right-click on surfaces barehanded to climb."
msgstr ""
"- Preso em um buraco? Segure o botão direito do mouse em superfícies com as "
"mãos nuas para escalar."

msgid "melt sand into glass"
msgstr "derreta areia em vidro"

msgid "melt down lode metal from lode cobble"
msgstr "derreta filão metálico a partir de pedregulho de filão"

msgid "make wet aggregate"
msgstr "faça agregado úmido"

msgid "make fire by rubbing sticks together"
msgstr "faça fogo esfregando gravetos"

msgid "make an anvil by tempering a lode cube"
msgstr "faça uma forja temperando um cubo de filão"

msgid "lux-infuse a lode tool"
msgstr "infusa uma ferramenta de filão com lux"

msgid "light a torch"
msgstr "acenda uma tocha"

msgid "insert wooden pin into wooden door panel"
msgstr "insira um pino de madeira em um painel de porta de madeira"

msgid "insert metal rod into a cobble panel"
msgstr "insira uma haste de metal em um painel de pedregulho"

msgid "harvest a sponge"
msgstr "colha uma esponja"

msgid "hammer lode bars into a rod"
msgstr "marrete barra de filão em uma haste"

msgid "hammer a lode prill into a bar"
msgstr "marrete granulado de filão em uma barra"

msgid "hammer a lode bar back to a prill"
msgstr "marrete uma barra de filão de volta em granulado"

msgid "grind leaves into peat"
msgstr "triture folhas em turfa"

msgid "find molten rock"
msgstr "encontre pedra derretida"

msgid "find lux"
msgstr "encontre lux"

msgid "find lode ore"
msgstr "encontre minério de filão"

msgid "find dry (loose) leaves"
msgstr "encontre folhas (soltas) secas"

msgid "find deep stone strata"
msgstr "encontre estrato de pedra profunda"

msgid "find charcoal"
msgstr "encontre carvão mineral"

msgid "find ash"
msgstr "encontre cinzas"

msgid "find an eggcorn"
msgstr "encontre uma pinha"

msgid "find a stick"
msgstr "encontre um graveto"

msgid "find a sponge"
msgstr "encontre esponja"

msgid "find a lode stratum"
msgstr "encontre um estrato de filão"

msgid "ferment peat into humus"
msgstr "fermente turga em húmus"

msgid "extract living sponge from colony center"
msgstr "extraia uma esponja viva do centro de uma colônia"

msgid "dry out a sponge"
msgstr "drene uma esponja"

msgid "dig up stone"
msgstr "escave pedra"

msgid "dig up sand"
msgstr "cave areia"

msgid "dig up lux cobble"
msgstr "cave pedregulho de lux"

msgid "dig up lode ore"
msgstr "cave minério de filão"

msgid "dig up gravel"
msgstr "cave cascalho"

msgid "dig up dirt"
msgstr "cave terra"

msgid "dig up a tree stump"
msgstr "desenterre um cepo de madeira"

msgid "cut down a tree"
msgstr "corte uma árvore"

msgid "craft a torch from staff and coal lump"
msgstr "construa uma tocha a partir de um bastão e um nódulo de carvão"

msgid "cool molten glass into crude glass"
msgstr "esfrie vidro derretido em vidro bruto"

msgid "cold-forge lode down completely"
msgstr "forge filão completamente, de forma fria"

msgid "cold-forge annealed lode prills into a tool head"
msgstr ""
"faça uma cabeça de ferramenta forjando friamente granulado de filão recozido"

msgid "chop a lode rod back into bars"
msgstr "martele uma haste de veio mineral de volta em barras"

msgid "Adze"
msgstr "Enxó"

msgid "(C)2018-2020 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2020 por Aaron Suen <war1024@@gmail.com>"

msgid "+"
msgstr "+"

msgid "Artificial Water"
msgstr "Água Artificial"

msgid "Amalgamation"
msgstr "Amalgamação"

msgid "Adobe Mix"
msgstr "Mistura de Adobe"

msgid "Adobe"
msgstr "Adobe"

msgid "@1 |||||"
msgstr "@1 |||||"

msgid "@1 ||||."
msgstr "@1 ||||."

msgid "@1 |||.."
msgstr "@1 |||.."

msgid "@1 ||..."
msgstr "@1 ||..."

msgid "@1 |...."
msgstr "@1 |...."

msgid "@1 ....."
msgstr "@1 ....."

msgid "@1 (90@2)"
msgstr "@1 (90@2)"

msgid "@1 (9)"
msgstr "@1 (9)"

msgid "@1 (80@2)"
msgstr "@1 (80@2)"

msgid "@1 (8)"
msgstr "@1 (8)"

msgid "@1 (70@2)"
msgstr "@1 (70@2)"

msgid "@1 (7)"
msgstr "@1 (7)"

msgid "@1 (60@2)"
msgstr "@1 (60@2)"

msgid "@1 (6)"
msgstr "@1 (6)"

msgid "@1 (50@2)"
msgstr "@1 (50@2)"

msgid "@1 (5)"
msgstr "@1 (5)"

msgid "@1 (40@2)"
msgstr "@1 (40@2)"

msgid "@1 (4)"
msgstr "@1 (4)"

msgid "@1 (30@2)"
msgstr "@1 (30@2)"

msgid "@1 (3)"
msgstr "@1 (3)"

msgid "@1 (20@2)"
msgstr "@1 (20@2)"

msgid "@1 (2)"
msgstr "@1 (2)"

msgid "@1 (10@2)"
msgstr "@1 (10@2)"

msgid "@1 (100@2)"
msgstr "@1 (100@2)"

msgid "- Some recipes use a 3x3 "grid", laid out flat on the ground."
msgstr ""
"- Algumas receitas usam uma \"grade\" 3x3, disposta diretamente sobre o chão."

msgid "- Larger recipes are usually more symmetrical."
msgstr "- Receitas maiores geralmente são mais simétricas."

msgid "- Hold/repeat right-click on walls/ceilings barehanded to climb."
msgstr ""
"- Segure/repita o clique direito em paredes/tetos para escalar com mãos nuas."

msgid "- Do not use F5 debug info; it will mislead you!"
msgstr "- Não use as informações de debug do F5; Apenas irão lhe confundir!"

msgid "Bindy Pliant Tarstone"
msgstr "Pedra de Alcatrão Flexível Liguenta"

msgid "Bindy Pliant Stone"
msgstr "Pedra Flexível Liguenta"

msgid "Bindy Pliant Sandstone"
msgstr "Arenito Flexível Liguento"

msgid "Bindy Pliant Adobe"
msgstr "Adobe Flexível Liguenta"

msgid "Bindy Adobe"
msgstr "Adobe Liguento"

msgid "Bindy"
msgstr "Liguento"

msgid "Bindy Tarstone"
msgstr "Pedra de Alcatrão Liguenta"

msgid "Bindy Stone"
msgstr "Pedra Liguenta"

msgid "Bindy Sandstone"
msgstr "Arenito Liguento"

msgid "Boxy Tarstone"
msgstr "Pedra de Alcatrão Quadrada"

msgid "Boxy Stone"
msgstr "Pedra Quadrada"

msgid "Boxy Pliant Tarstone"
msgstr "Pedra de Alcatrão Flexível Quadrada"

msgid "Boxy Sandstone"
msgstr "Arenito Quadrada"

msgid "Boxy Pliant Stone"
msgstr "Pedra Flexível Quadrada"

msgid "Boxy Pliant Sandstone"
msgstr "Arenito Flexível Quadrado"

msgid "Boxy Pliant Adobe"
msgstr "Adobe Flexível Quadrado"

msgid "Boxy Adobe"
msgstr "Adobe Quadrado"

msgid "Boxy"
msgstr "Quadradão"

msgid "Bonded Stone Bricks"
msgstr "Tijolos de Pedra Ligados"

msgid "Blank"
msgstr "Vazio(Em Branco)"

msgid "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr "- Desconfie de cavernas/abismos escuros; você é responsável por sair."

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr ""
"- Pontos de escalada também produzem luz muito fraca; aumentar a gama de "
"exibição para ver."

msgid "- Climbing spots may be climbed once black particles appear."
msgstr ""
"- Pontos de escalada podem ser escalados assim que as partículas pretas "
"aparecerem."

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr "- Nós deslocados podem ser escalados como pontos de escalada."

msgid "- Drop and pick up items to rearrange your inventory."
msgstr "- Solte e pegue itens para reorganizar seu inventário."

msgid "- For larger recipes, the center item is usually placed last."
msgstr ""
"- Para receitas maiores, o item central geralmente é colocado por último."

msgid "Azure Bell Flower"
msgstr "Flor de Sino Azul"

msgid "Black Bell Flower"
msgstr "Flor Sino Preta"

msgid "- FUTURE: @1"
msgstr "- FUTURO: @1"

msgid "- Hopelessly stuck? Try asking the community chatrooms (About tab)."
msgstr ""
"- Irremediavelmente preso? Tente perguntar às salas de bate-papo da "
"comunidade (Sobre a aba)."

msgid "- If it takes more than 5 seconds to dig, you don't have the right tool."
msgstr ""
"- Se demorar mais de 5 segundos para cavar, você não tem a ferramenta certa."

msgid "- Learn to use the stars for long distance navigation."
msgstr "- Aprenda a usar as estrelas para navegação de longa distância."

msgid "- Nodes dug without the right tool cannot be picked up, only displaced."
msgstr ""
"- Nós escavados sem a ferramenta certa não podem ser pegos, apenas "
"deslocados."

msgid "- To run faster, walk/swim forward or climb/swim upward continuously."
msgstr ""
"- Para correr mais rápido, ande/nade para a frente ou suba/nade para cima "
"continuamente."

msgid "- Tools used as ingredients must be in very good condition."
msgstr ""
"- As ferramentas usadas como ingredientes devem estar em muito boas "
"condições."

msgid "@1 (@2)"
msgstr "@1 (@2)"

msgid "@1 discovered, @2 available, @3 future"
msgstr "@1 descoberto, @2 disponível, @3 futuro"

msgid "Additional Mods Loaded: @1"
msgstr "Adicionais Mods Carregados: @1"

msgid "Adobe Bricks"
msgstr "Tijolos Adobe"

msgid "Annealed Lode Adze"
msgstr "Enxó de Veio Mineral Recozido"

msgid "Annealed Lode Rake"
msgstr "Ancinho de Veio Mineral Recozido"

msgid "Azure Cluster Flower"
msgstr "Flor de Cacho Azul"

msgid "Azure Cup Flower"
msgstr "Flor de Copo Azul"

msgid "Azure Rosette Flower"
msgstr "Flor Roseta Azul"

msgid "Azure Star Flower"
msgstr "Flor Estrela Azul"

msgid "Annealed Lode Frame"
msgstr "Quadro de Veio Mineral Recozido"

msgid "Annealed Lode Ladder"
msgstr "Escada de Veio Mineral Recozida"

msgid "CHEATS ENABLED"
msgstr "TRAPAÇAS ATIVADAS"

#, fuzzy
msgid "Discord: https://discord.gg/NNYeF6f"
msgstr "Discord: https://discord.gg/NNYeF6f"

msgid "Early-access edition of NodeCore with latest features (and maybe bugs)"
msgstr ""
"Edição de acesso antecipado de NodeCore com os recursos mais recentes (e "
"bugs mais recentes)"

#, fuzzy
msgid "IRC: #nodecore @@ irc.libera.chat"
msgstr "IRC: #nodecore @@ irc.libera.chat"

msgid "work glowing lode on a stone anvil"
msgstr "trabalhar com um filão quente em uma bigorna de pedra"

#, fuzzy
msgid "write on a surface with a charcoal lump"
msgstr "escrever em uma superfície com uma pedra de carvão"
