========================================================================
IDEAS: Possible future additions/improvements to the game
------------------------------------------------------------------------

  ####   #####    ####      #    #       ######  #####    ####
 #       #    #  #    #     #    #       #       #    #  #
  ####   #    #  #    #     #    #       #####   #    #   ####
      #  #####   #    #     #    #       #       #####        #
 #    #  #       #    #     #    #       #       #   #   #    #
  ####   #        ####      #    ######  ######  #    #   ####

- Accessibilty Options Megaproject

	- Consider merging copygirl's extended rotation
		- https://git.mcft.net/copygirl/nc_extended_rotating
		- Unlicense, (probably) does not need to be cleanroomed
		- The early design sounded a bit questionable, but it seems like
		in practice it actually works well
			- Too confusing to "plan" rotations?  It allows enough
			different axes that that's not much of an issue.
			- Too much precise control necessary?  Seems like it's
			not much of an issue and we could allow something like
			aux+rightclick to override and go with the old "just
			cycle them all" system.
		- The "highly buried optic" use-case seems to be a likely non
		issue.  It seems like it would work fine with simply-buried
		optics (we can access one whole face)

	- Consider merging a limited version of "extended placement"
		- https://content.minetest.net/packages/gamefreq0/extended_placement/
		- MIT for code, only cleanroom the media
		- Could be very valuable for cases like "place past the side of
		my bridge" for skyblock without falling off due to lagspikes.
		- Would need to limit some cases, e.g. building below an existing
		node, to prevent cheesing skyrealm
		- Adds another "precision control" requirement that would need
		to be bypassable.

	- Add an "options" screen to player GUI
		- The ability to toggle some things, ideally just simple on/off
		to start with.
			- Maybe dropdown/enum is the ideal control to use (e.g.
			  for things that offer finite modes that aren't
			  easily composable from independent yes/no choices)
		- Mods can register additional options to go in there
		- To use for things like:
			- Advanced placement/rotation options based on whether
			player has enough precision control
			- Pummel assistance: punch twice in rapid enough succession
			and then stare at the same spot to complete pummel, for
			players with hand mobility issues
			- Visual subtitles (currently languishing in a side branch
			due to how disruptive/distracting they would be if unable
			to turn them off)

	- Visual "subtitles" for sounds?
		- Experiments in the "subtitles" branch.
		- Use new image_waypoint HUD type; it works in new versions,
		invisible in old versions (graceful fallback).
		- An icon for each sound type.

- Incorporate some existing mods:
	- ncbells can mostly already go in as it is
		- Green wanted to fix some sound quality issues
		- Would be nice to have a way to control volume
			- Material of stick, door tier?
			- Attach lode things like rods, frames, cubes to bottom?
	- ZipRunes are de facto essential on servers anyway
		- Using charcoal doesn't feel quite right though
		- Maybe something involving glass?
		- Take advantage of the fact that the player is
		  dreamlike/noncorporeal to not sacrifice realism here

- Leaping sparks/podobos from lava
	- Rarely triggered by pumwater, with per-node cooldown.
	- Require at least one pum neighbor
		- Small controlled setups like skyblocks don't have unfair surprises
	- Chance of happening is reduced by nearby pum (check a random nearby node, if pum, cancel)
		- This compensates for naturally increased rate of large pum lakes
	- If it does happen, pick 2+ random nearby nodes and trigger them, to cause a chain reaction
		- Though large lakes dont erupt as often, it's more spectacular
	- Hookable/overridable so mods can add effects; include chain reaction generation in callback
		- Especially for WinterCore Vulcan mod
	- Make chain reaction ones shoot higher?
	- Make them bounce if they land on solids?
	- Set fires where they touch.
	- Possibly knock stone loose and cause cave-ins?  Displacing
		lava lakes upward would be pretty dramatic.

- Should packed totes transmit light from items inside?
	- Maybe at reduced intensity?
	- Maybe need an on_get_light_source intelligent API for this
	  or something...
	- Need to make sure torches and lanterns properly run down
	  while inside the tote and don't just "catch up" when unpacked

- More nuanced infrared attenuation/scattering
	- Float glass vs clear glass should have some differences

- Improve lode ore hint-stains
	- Make sure hint stone always indicates at least some real ore
	- Generate ore veins in mapgen
	- In post-gen, find ore, spread stone stains outward in a few
	  random direction streaks

- Wicker baskets
	- Limited alternative to full tote
	- Made of cheaper materials (wicker, wood form)
	- Can only carry wooden forms (must be unpacked on a flat surface)
	- Very flammable, ejects all contents (nodes and stacks) upon
	  ignition
	- Make them only contain certain items, others (e.g. sand/ash)
	  filter/leak through?
	- Make them drop items periodically?

- Clean up sky pummesses
	- Make free-floating pumice randomly fall?  melt?  decay?
	- Make pumice trigger chain melt reactions?
	- Only if not already on the ground?

- Aux-punch to take 1 item out of a stack?

- Discovery Expansion
	- Support for "hidden" hints that never show up as hidden/future, but
	  only appear when completed.
		- For bonus/hidden content like renewal recipes.
	- Allow hints to be assigned IDs/groups, used as prereqs themselves.
		- Would need a deferred/multipass system for testing.
	- Maybe show hidden hints once all non-hidden ones are done?

- Half hint completion from witnessing?
	- Some hints are witness-only like leaching, you just get full
	  completion for those
	- Some hints can be done by hand, or witnessed; allow a special
	  half-completion state for those
	- Gives players hints that they saw something worth doing, but
	  let them keep track of the fact that they haven't figure it out
	  themselves

- Add "welcome" messages on login
	- In the same spirit as the server version line
	- Notify if NodeCore updated since last played
	- Welcome new players?
	- Warn on special conditions, e.g. world in stasis?
		 - Add to /status?

- Hint system "quest" lines?
	- Series of hints specially marked.
	- Special game significance.
	- Examples:
		- Tool tiers
		- Automation

- Make infused tools not directly flammable, BUT lose durability when
  near igniters similar to water effect.

- Underground generated mazes, more compact than dungeons
	- Made of harder stone
	- Hide prizes like ores, treasures, etc.

- Stone brick hardening (in-place)?

- Use stylus on sand to produce same patterns as on concrete, inverted?

- Display some kind of warning on >2s lagspikes
	- This breaks some game things like pummels
	- Players should all be warned ... HUD?

- More social particles
	- On digging
	- On pummel (don't just show to one player)

- More door stuff
	- More materials
		- Wicker?
		- Forms of glass?
		- Lode?
	- High tier door ideas
		- Require higher-tier doors to operate higher-tier
		tools/heads?
		- Limit ability of lower-tier doors to drive higher-tier
		doors, e.g. only allow driving up to 1 tier above?  This
		could allow one-way slippage mechanisms.
		- Some doors can't be hand-operated, only ablated.

- Plants from nc_nature
	- To import:
		- Mushrooms
	- Function:
		- Source of special materials like ash? Cheaper peat?
		- Upconvert dirt to humus nearby?

- Magnets or other items to help find metal, especially locating
  lost valuable tools, totes, etc?

- Durable transparent/window material.
	- Harder to mine, noob protection.
	- "Reinforced glass?"
		- Use lava+water hardening recipe on glasses?
	- Lode frames to fill this role?
		- Reach-through-able lux rad shielding?

- Make radiant heat do burn damage instead of plain?
	- Reuse lux burns?  Different kind of burn?

- Spindle/axle nodes
	- Can carry door rotations along a 1x1 path
	- Don't actually move (except maybe animate)
	- Apply friction in all 4 directions
	- Can act as gears.
	- Note that they should "spin" twice as fast, though this
	  probably wouldn't actually matter according to actual mechanics.
	- Maybe make threading?
		- Apply lateral forces to certain nodes?
		- Make grain augers?
		- Special threaded elevator nodes?
	- Clutch nodes to rotate nodes on ends
		- Rotate facedir nodes
		- Make storeboxed rotatable so we can dump them out

- Make etched concrete rotatable?
	- Rotate only in pliant state by rightclick.
	- When dug w/ silk touch, store param2 in metadata, add rotation
	  value to touchtip to explain non-stacking.

- Buff igniting
	- Make hot lode and other heated materials able to ignite
		- This was probably not already done just because we
		  didn't have AISMs at the time, but we do now.
	- Reconsider lux fluid ignition again.
		- Maybe have rare "spark" events that can ignite?
		- Maybe have it involve proximity to other materials?
			- Water? Lava?
			- Optic beams?

- Can we use glasslike_framed for shelves?
	- Alternate 2 versions of the registration based on y coord
	  so it forces top/bottom faces to show

- Sealed storeboxes?
	- Pummel glass onto case, planks onto shelf
	- No side access at all until unsealed

- More lode bar crafts:
	- Allow frame loose ends to be chopped off when placed next to other
	  frames, so custom shapes can be made.

- Chop wood ladders/frames back into sticks?

- Piezo node for optics detects sounds?
	- Sounds emitted by mod stuff?
	- Footsteps?
	- Can be used as a BUD

- Lux/lode ore in already-partly-dug cobble form in mapgen/ore?

- Dungeon loot
	- Expose API for mods to intercept/modify loot
	- Add recipe hints (prearranged nodes) as decorations?

- A lode "mace" tool with all dig groups?
	- Would require reevaluatng recipe priority, or tool modality.

- Make lava easier to find at a distance visually
	- Particles, like cherenkov?

- New Darkness feature:
	- Keep track of queue of recent "safe spots" for player:
	  in sunlight, on solid ground or climbable etc.
	- Also check for being completely entombed in solid non-climbables.
	- Track player darkness, search for nearby light.  Keep track of recent hit/miss (ring/queue).
	- If player is in TOTAL darkness for long enough to be certain, then do "fugue" teleportation.
	- Fade screen to black slowly via HUD (maybe have an API for this for skyrealms).
	- Drop all items.
	- Apply a temporary debuff...?
	- Teleport player back to recent safe spots until we find one, or run out.
	- Fade screen back from black.

- Should tool appearance vary based on wear?
	- Make "fresh" vs. "worn" tools (for purposes of their usability
	  in crafts) have a distinct look?
	- Move the wear stat out of normal wear and into metadata, so the bar
	  graphs don't show.
		- Would this require reworking the whole damage/uses system?
	- Tools used in crafts will need to be groups instead of item names.

- Tree sap
	- Stumps with air above exude, become dry stumps.
	- Use as a glue/resin in recipes.
	- Make longer-life torches?
	- Stick optics in place to prevent rotating?

- Should ash and derivatives have a fertilizing effect?
	- Maybe wet/tacky stuccos?

- Way to detect specific nodes
	- Comparison against a sample node?
	- Allow optic signals to carry "chroma" signatures, compare?

- Renewability Gap
	- Renewable but not generatable
		- Sponges: Sprout under water from peat+sand?
		- Lode
		- Lux cobble
		- Water sources
		- Lava sources
		- Sedges
		- Rushes
		- Flowers
	- Sustainable but not renewable
	- Not sustainable
		- Lux stone (smooth)

- Threats
	- Visceral
		- Flammable/toxic gas?
		- Monsters: stone-lurkers, mimics.
			- Complex multi-node mimic that eats tools you try to dig it with,
			  stores them in core node(s) inside somewhere to be recovered.
		- Lightning, meteor strikes?
	- Creeping
		- Cellular automata hazards.
		- From exploration, delving too deep, leaving things to rot, etc.
		- Blights, Fungi
		- Termites, carpenter ants, other wood-eating inflictions?

- Lurk Ore
	- Moves freely among air-exposed stone while no player is looking.
	- Follows after player, attempting to cause harm.
		- Weakens nodes above/below player into falling nodes,
		  e.g. loose cobble, gravel?
		- Steals items from player, drops onto ground or absorbs them
		  into body?  Saps tool durability?
		- Ignites flammables it passes by?
		- Downgrades or absorbs ores nearby by contact or air floodfill?
		- Creates illusionary nodes you can fall through?
	- Cannot be dug or damaged directly.
		- Digging it yields plain stone, but converts up to 2
		  surrounding stone nodes into Lurk.
	- Need to dig all around it so it cannot move, then apply Some
	  time-integrated process to convert it to useful form.

- Chop (menger) sponges into (sierpinski) carpets?
	- Walk-through thin facade nodes, like charcoal glyphs
	- Block liquid flow paths
	- Hold moisture, act as non-blocking coolant?

- New materials to craft with.
	- Dungeon materials, stonework?
	- Decorations for dungeons
	- Small plants? Reeds? Mallows?
	- Sea stars, anenome, coral, other underwater things?
	- Fungi, esp. tree-destroying ones, blight?
	- Oil, natural gas?  Fossils and fossil fuels?
	- Geode, hydra crystals, corundum?
	- Shipwrecks or alien tech
	- Slow-moving animals? Snails? Miniature spice worms?
	- Non-portable things, like "spawners" or wormholes
	- Tubers and taproots, cacti, and other "defensive" plantlife
	- Plant-like CA animals, like bee nests and clouds of bees?
	  Termine mounds? Ant colonies? Coral?
	- Popeggcorn?  Like a plank but falling_node?
	- Ores that smelt via heating and then rapid quenching?
	- Things that cannot ever be dug, and must be moved only
	  by in-world machinery?

- Vary player walking speed based on nodes under feet?

- Visual "craft guide" system?
	- Build a 3x3 work space out of wood/logs, place center node
	  last and it will convert to a workbench node.
	- Workbench node will scan for potential recipe matches
	  centered on the space above based on what's present
	  and matches at least 1 non-air node, picks one.
	- Create display "ghost" entities representing items that
	  can be placed to complete the recipe.

- Should tree growth rate start out faster but slow down?
	- This could make the decision to harvest trees early vs
	  wait for them to mature more nuanced.

- Social features
	- Hint system
		- Social achievments
			- When completing a hint, also tell other players on
			  the same server if they've completed it too.
		- Vicarious discovery
			- When a player completes a hint/discovery, generate
			  witness events for anyone who can see.
	- Randomize player appearance/colors.
		- Skin color, hair color, eye color?
	- Add a "cloak" layer for cloaks, capes, over entire body
	- Make some control states / anims last a little longer, e.g.
	  extend mining or waving anims to a second or so to make sure
	  they're visible and not just flicker.

- Drop-in recipes, triggered on thrown or falling items settling?
	- Good for dangerous stuff, maybe?

- Compact signage via concrete/writing/stylus
	- Draw glyphs on a wall.
	- Place a lens focusing light from the wall onto a spot.
	- Put pliant concrete in that spot.
	- Make sure wall is adequately lit.
	- Etch the lens-facing side of the pliant concrete with stylus.
- Create a "custom patterned" concrete sign block with letters etched
  onto it.  Store in metadata, use ents to render, etc.

- Door animations.
	- These need to be relatively efficient, i.e. not involve
	  a ton of node manipulation or spawning lots of entities.
	  Of particular concern is network packet load on clients.
	- Something using animated entities (which could represent
	  multiple nodes sharing an axis) could work well.
	- Alternatively, a reasonable particle effect?  Something
	  abstract is fine too, as long as it conveys a sense of
	  movement to help players visualize what's moving and in
	  what direction.

- Visual in-world hotbar
	- GreenXenith's visualbar mod
	- Replace hotbar, merge it with both bandolier and YCTIWY?
	- Players could be "pickpocketed" during normal gameplay.
		- Make a sound/visual to warn player.
	- Jordach's new first-person-attached entities feature?
	- Avicennia's Satchel mod?
	- "Yoshi's Island" chain of followers?

- Mod removal substitution system
	- Mods can register their content with fallback item lists
	  that are saved in worlddir
	- If mod is removed, items are replaced with their fallbacks
	  by the game to avoid unknown_nodes.
	- More complex items can register ingredients for "uncrafting"
	  and automatic recycling; could work recursively.
		- Eject multiple things as items like skyrealm items.

........................................................................
========================================================================
